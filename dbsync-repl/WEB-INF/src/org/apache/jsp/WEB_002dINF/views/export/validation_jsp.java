/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: JspC/ApacheTomcat8
 * Generated at: 2018-06-12 14:29:05 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.WEB_002dINF.views.export;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class validation_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
      throws java.io.IOException, javax.servlet.ServletException {

    final java.lang.String _jspx_method = request.getMethod();
    if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
      response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET POST or HEAD");
      return;
    }

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n\n\n<style>\n    .row-margin {\n        margin-left: 1px;\n        margin-right: 1px;\n    }\n\n    .logs-content {\n        height: 70%;\n        overflow: auto;\n    }\n\n    .logs-control {\n        resize:none;\n        box-sizing: border-box;\n        height: 100% !important;\n        overflow: auto;\n    }\n\n    .form-content {\n        height: 100%;\n    }\n\n    .small {\n        height: 0;\n        overflow: hidden;\n    }\n\n    .big {\n        height: auto;\n        color: red;\n    }\n\n    .unmatched {\n        font-style: italic;\n        color: red;\n    }\n\n    .matched {\n        font-style: italic;\n        color: green;\n    }\n</style>\n\n<script>\n    $(document).ready(function () {\n        loadLogs();\n        $(document).ajaxStart(function() { Pace.restart(); });\n    });\n\n    function expandDetails(self) {\n        var wrapper = $(self).parent().next().next();\n        var small = wrapper.find('.small');\n        var big = wrapper.find('.big');\n\n        if(small && small.size()) {\n            small.toggleClass('small big');\n        } else if(big && big.size()) {\n");
      out.write("            big.toggleClass('big small');\n        }\n    }\n\n    function loadLogs() {\n        var result;\n        var html = '';\n\n        html += \"<span>Interpreting source schema... </span><br/>\";\n        html += \"<span>Extracting target schema... </span><br/>\";\n\n        try {\n            result = JSON.parse('");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${validationResult}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("');\n\n            if(result.connectionEstablished) {\n                result.unmatchedObjects.forEach(function(object) {\n                    html += \"<span>Object \" + object + \" is not found in target schema</span><br/>\";\n                });\n\n                for(var prop in result.objects) {\n                    if(result.objects.hasOwnProperty(prop)) {\n                        html += \"<span>Matching fields of \" + prop + \"... </span><br/>\";\n\n                        if(result.objects[prop].length) {\n                            html += '<span class=\"unmatched\">The following fields are missing in target Salesforce ' +\n                                'and will be ignored when making a copy.</span><br/>';\n\n                            html += '<span class=\"unmatched\">Found  <a onclick=\"expandDetails(this);\">' +\n                                result.objects[prop].length +\n                                ' </a> unmatched ' + prop +\n                                ' fields in target schema </span><br/>';\n\n                            html += '<div class=\"wrapper\"><div class=\"small\"><p>';\n");
      out.write("                            html += result.objects[prop].join(', ');\n                            html += '</p></div></div>';\n                        } else {\n                            html += '<span class=\"matched\">All fields matching.</span><br/>';\n                        }\n                    }\n                }\n            } else {\n                html += '<span class=\"unmatched\">Cannot interpret target schema </span><br/>';\n            }\n\n            html += \"<span>Validation finished. </span><br/>\";\n\n            $('#logs').html(html);\n        } catch (e) {\n            toastr.error('Internal error. Please contact us.');\n        }\n    }\n\n    function cancel() {\n        $('#exportModal').modal('hide');\n    }\n\n    function setValidationParameters() {\n        $('input[type=hidden][name=metadataObjects]').val(JSON.stringify(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${metadataObjects}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("));\n        $('input[type=hidden][name=includeDeleted]').val(JSON.stringify(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${includeDeleted}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("));\n    }\n\n    function goToNotification(self) {\n        setValidationParameters();\n        loadAndShowModal('Notification','export/notification', self, 3);\n    }\n</script>\n\n<form id=\"validationForm\" class=\"white-bg form-content\" method=\"post\">\n\t<input type=\"hidden\" name=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${_csrf.parameterName}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${_csrf.token}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\" />\n    <input type=\"hidden\" name=\"sourceProfile\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${sourceProfile}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\">\n    <input type=\"hidden\" name=\"targetProfile\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${targetProfile}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\">\n    <input type=\"hidden\" name=\"metadataObjects\">\n    <input type=\"hidden\" name=\"includeDeleted\">\n\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <div id=\"logs\" class=\"form-control logs-control bg-white\">\n            </div>\n        </div>\n        <div class=\"col-sm-12\">\n            <a id=\"validationNext\" class=\"btn btn-primary pull-right\" style=\"display: none;\"\n               onclick=\"goToNotification(this);\">\n                Next\n            </a>\n        </div>\n    </div>\n</form>\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
