/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: JspC/ApacheTomcat8
 * Generated at: 2018-06-12 14:29:05 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.WEB_002dINF.views.export;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.List;

public final class listprofiles_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = new java.util.HashSet<>();
    _jspx_imports_classes.add("java.util.List");
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005furl_0026_005fvalue_005fnobody;

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fc_005furl_0026_005fvalue_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fc_005furl_0026_005fvalue_005fnobody.release();
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
      throws java.io.IOException, javax.servlet.ServletException {

    final java.lang.String _jspx_method = request.getMethod();
    if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
      response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET POST or HEAD");
      return;
    }

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write('\n');
      out.write("\n\n\n\n");

	List<String> profileList = (List<String>) request.getAttribute("profileList");

      out.write("\n<style>\n\t.panel-content {\n\t\theight: 100%;\n\t\toverflow: auto;\n\t}\n</style>\n\n<script>\n\t$(document).ready(function () {\n        var $profileSelected = $(\"#profile_selected\");\n        \n        currObj = $profileSelected;\n\t\ttargetProfile = currObj.val();\n\t\tnewProfile = '';\n\t\tpanel = 1;\n\n\t\t$profileSelected.on(\"change\", function(event) {\n\t\t\ttargetProfile = $(event.target).val();\n        });\n\t\t\n\t\t$(\"#firstPanel\").on(\"click\", function() {\n\t\t\tpanel = 1;\n\t\t\tvar $profileSelected = $(\"#profile_selected\");\n\t\t\tcurrObj = $profileSelected;\n\t\t\ttargetProfile = currObj.val();\n\t\t});\n\t\t\n\t\t$(\"#secondPanel\").on(\"click\", function() {\n\t\t\tpanel = 2;\n\t\t\t$(\"#profile_selected\").val(\"-1\");\n\t\t\tcurrObj = $(\"#new_sf_page\");\n\t\t\tcurrObj.load('");
      out.print( request.getContextPath());
      out.write("/export/sf');\n\t\t\ttargetProfile = newProfile;\n\t\t});\n\t});\n\n    function setProfileParameters(selected_profile) {\n    \t$('input[type=hidden][name=includeDeleted]').val($('#isDeletedProp').prop(\"checked\"));\n        $('input[type=hidden][name=metadataObjects]').val(JSON.stringify(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${metadataObjects}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("));\n        $('input[type=hidden][name=targetProfile]').val(selected_profile);\n    }\n\n    function goToValidation() {\n    \tsetProfileParameters(targetProfile);\n    \tvar formObj =  $(currObj).closest(\"form\");\n        loadAndShowModal('Validate','export/validation', formObj, 2);\n    }\n\n    function loadProfile(profileName) {\n    \tvar deferred = $.Deferred();\n    \tvar jsonString = {};\n    \t\n    \t$.ajax({\n\t\t\tasync : false,\n\t\t\ttype : \"GET\",\n\t\t\turl : \"");
      if (_jspx_meth_c_005furl_005f0(_jspx_page_context))
        return;
      out.write("\" + profileName,\n\t\t\tcache : false,\n\t\t\tsuccess : function(data) {\n\t\t\t\tjsonString = data;\n\t\t\t\tdeferred.resolve(jsonString);\n\t\t\t},\n\t\t\terror : function(xhr, status, error) {\n    \t\t\tif (xhr.status === 200) {\n    \t\t\t\ttoastr.error(\"Session expired. Please login once again\");\n    \t\t\t} else {\n    \t\t\t\ttoastr.error(\"There seem to be some technical problem. Please try saving again\");\n    \t\t\t}\n    \t\t\t\n    \t\t\tdeferred.resolve(jsonString);\n    \t\t},\n    \t\tcomplete: function () {\n                deferred.resolve(jsonString);\n            }\n\t\t});\n\t\t\n\t\treturn deferred.promise();\n    }\n    \n\tfunction testAndValidateSF(isSave) {\n\t\tvar deferred = $.Deferred();\n\t\tvar validBtnText = \"\", error = \"\";\n\t\tvar ret = false;\n\t\tvar bool = false;\n\t\tvar profileName = \"\";\n\t\tun = \"\";\n\t\tpwd = \"\";\n\t\turl = \"\";\n\t\tconkey = \"\";\n\t\tconsecret = \"\";\n\t\toAuthurl = \"\";\n\t\tvar oAuthConnection=\"false\";\n\t\t\n\t\tif(isSave === \"true\") {\n\t\t\tun = document.getElementById('PROP.salesforce.username').value;\n\t\t\tpwd = document.getElementById('PROP.salesforce.password').value;\n");
      out.write("\t\t\turl = document.getElementById('PROP.soap.endpoint').value;\n\t\t\tconkey = document.getElementById('salesforce.accessToken').value;\n\t\t\tconsecret = document.getElementById('salesforce.refreshToken').value;\n\t\t\toAuthurl = document.getElementById('PROP.salesforce.callbackurl').value;\n\t\t\toAuthConnection= document.getElementById('OAuthConnection').value;\n\t\t} else {\n\t\t\tprofileName = $('#profile_selected').val();\n\t\t\tconsole.log(\"Profile Name : \" + profileName);\n\t\t\t\n\t\t\tvar profileJson = {};\n\t\t\tvar profileProps = loadProfile(profileName);\n\t\t\tprofileProps.done(function(result) {\n    \t        console.log(\"profileJsonString : \" + result);\n    \t        profileJson = result;\n    \t    });\n\t\t\t\n\t\t\tif(profileJson.hasOwnProperty(\"salesforce.username\")) {\n\t\t\t\tun = profileJson[\"salesforce.username\"];\n\t\t\t}\n\t\t\tif(profileJson.hasOwnProperty(\"salesforce.password\")) {\n\t\t\t\tpwd = profileJson[\"salesforce.password\"];\n\t\t\t}\n\t\t\tif(profileJson.hasOwnProperty(\"soap.endpoint\")) {\n\t\t\t\turl = profileJson[\"soap.endpoint\"];\n\t\t\t}\n\t\t\tif(profileJson.hasOwnProperty(\"salesforce.accessToken\")) {\n");
      out.write("\t\t\t\tconkey = profileJson[\"salesforce.accessToken\"];\n\t\t\t}\n\t\t\tif(profileJson.hasOwnProperty(\"salesforce.refreshToken\")) {\n\t\t\t\tconsecret = profileJson[\"salesforce.refreshToken\"];\n\t\t\t}\n\t\t\tif(profileJson.hasOwnProperty(\"salesforce.callbackurl\")) {\n\t\t\t\toAuthurl = profileJson[\"salesforce.callbackurl\"];\n\t\t\t}\n\t\t\tif(profileJson.hasOwnProperty(\"OAuthConnection\")) {\n\t\t\t\toAuthConnection = profileJson[\"OAuthConnection\"];\n\t\t\t}\n\t\t}\n\t\t\n\t\tret = validateSF(un, pwd, isSave, oAuthConnection);\n\n\t\tif(ret === true){\n\t\t\t$.ajax({\n\t\t\t\ttype : \"POST\",\n\t\t\t\turl : \"");
      if (_jspx_meth_c_005furl_005f1(_jspx_page_context))
        return;
      out.write("\",\n\t\t\t\tcache : false,\n\t\t\t\tdata : {\n\t\t\t\t\t\t\tusername : un,\n\t\t\t\t\t\t\tpassword : pwd,\n\t\t\t\t\t\t\turl : url,\n\t\t\t\t\t\t\tdbType : 'sf',\n\t\t\t\t\t\t\tconsumerKey : conkey,\n\t\t\t\t\t\t\tconsumerSecret : consecret,\n\t\t\t\t\t\t\tcallbackUrl : oAuthurl,\n\t\t\t\t\t\t\tprofileName : profileName,\n\t\t\t\t\t\t\t_csrf : \"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${_csrf.token}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\"\n\t\t\t\t\t\t},\n\t\t\t\tsuccess: function (data1) {\n\t\t\t\t\tif(data1.trim() !== \"SUCCESS\") {\n\t\t\t\t\t\terror = \"Error : \" + data1;\n\t\t\t\t\t\ttoastr.error(error);\n\t\t\t\t\t\tconsole.log(\"Connection Failed with error : \" + error);\n\t\t\t\t\t\tbool = false;\n\t\t\t\t\t} else {\n\t\t\t\t\t\tconsole.log(\"Connection successful\");\n               \t\t\tbool = true;\n               \t\t\t\n               \t\t\tif(isSave === \"true\") {\n               \t\t\t\tvar saveResult = saveConfig();\n               \t\t\t\tsaveResult.done(function(result) {\n                    \t        console.log(\"Bool: \" + result);\n                    \t        bool = result;\n                    \t    });\n               \t\t\t}\n\t\t\t\t\t}\n\n\t\t\t\t\tdeferred.resolve(bool);\n\t            },\n\t            error : function(xhr, status, error) {\n\t    \t\t\tif (xhr.status == 200) {\n\t    \t\t\t\ttoastr.error(\"Session expired. Please login once again\");\n\t    \t\t\t} else {\n\t    \t\t\t\ttoastr.error(\"There seem to be some technical problem. Please try saving again\");\n\t    \t\t\t}\n\t    \t\t\t\n\t    \t\t\tdeferred.resolve(bool);\n\t    \t\t},\n\t    \t\tcomplete: function () {\n");
      out.write("\t                deferred.resolve(bool);\n\t            },\n\t\t\t\tasync : false\n\t\t\t});\n\t\t} else {\n\t\t\tdeferred.resolve(bool);\n\t\t}\n\t\t\n\t \treturn deferred.promise();\n\t}\n\t\n\tfunction saveConfig() {\n\t\tvar deferred = $.Deferred();\n\t\tvar formObj = $(currObj).closest(\"form\");\n\t\tvar bool = false;\n\t\t$.ajax({\n\t\t\tasync : false,\n\t\t\ttype : \"POST\",\n\t\t\turl : \"");
      if (_jspx_meth_c_005furl_005f2(_jspx_page_context))
        return;
      out.write("\",\n\t\t\tcache : false,\n\t\t\tdata : $(formObj).serialize(),\n\t\t\tsuccess : function(data1) {\n\t\t\t\tvar obj = JSON.parse(data1);\n\t\t\t\tif(obj.status.trim() === \"SUCCESS\") {\n\t\t\t\t\ttargetProfile = newProfile = obj.profile_new;\n\t\t\t\t\ttoastr.info(\"New profile created with name : \" + newProfile);\n\t\t\t\t\tbool = true;\n\t\t\t\t} else {\n\t\t\t\t\ttoastr.error(\"Error : \" + obj.errormsg);\n\t\t\t\t\tbool = false;\n\t\t\t\t}\n\t\t\t\t\n\t\t\t\tdeferred.resolve(bool);\n\t\t\t},\n\t\t\terror : function(xhr, status, error) {\n    \t\t\tif (xhr.status == 200) {\n    \t\t\t\ttoastr.error(\"Session expired. Please login once again\");\n    \t\t\t} else {\n    \t\t\t\ttoastr.error(\"There seem to be some technical problem. Please try saving again\");\n    \t\t\t}\n    \t\t\t\n    \t\t\tdeferred.resolve(bool);\n    \t\t},\n    \t\tcomplete: function () {\n                deferred.resolve(bool);\n            }\n\t\t});\n\t\t\n\t\treturn deferred.promise();\n\t}\n\t\n\tfunction validateSF(un, pwd, isSave, oAuthConnection){\n\t\tvar error = \"\";\n\t\tif(un === \"\" && pwd === \"\"){\n\t\t\tif(isSave === \"true\") {\n\t\t\t\terror = \"Error : Need UserName and Password \";\n");
      out.write("\t\t\t} else {\n\t\t\t\terror = \"Invalid Profile : Missing User Name and Password in the profile\";\n\t\t\t}\n\t\t} else if (un === \"\"){\n\t\t\tif(isSave === \"true\") {\n\t\t\t\terror = \"Error : Need UserName \";\n\t\t\t} else {\n\t\t\t\terror = \"Invalid Profile : Missing User Name in the profile\";\n\t\t\t}\n\t\t} else if(pwd === \"\" && oAuthConnection === \"false\"){\n\t\t\tif(isSave === \"true\") {\n\t\t\t\terror = \"Error : Need Password\";\n\t\t\t} else {\n\t\t\t\terror = \"Invalid Profile : Missing Password in the profile\";\n\t\t\t}\n\t\t}  else {\n\t\t\treturn true;\n\t\t}\n\t\t\n\t\ttoastr.error(error);\n\t\treturn false;\n\t}\n</script>\n\n<div class=\"row\">\n\t<div class=\"col-lg-12\">\n\t\t<div class=\"panel-body panel-content\" style=\"background-color: #ffffff;\">\n\t\t\t<div class=\"panel-group\" id=\"accordion\">\n\n\t\t\t\t<label class=\"btn btn-default\" style=\"border:none;\">\n\t\t\t\t\t<input type=\"checkbox\" id=\"isDeletedProp\" style=\"opacity: 2;\"/>\n\t\t\t\t\tInclude Deleted Records\n\t\t\t\t</label>\n\n\t\t\t\t<div class=\"panel panel-default\">\n\t\t\t\t\t<div class=\"panel-heading\">\n\t\t\t\t\t\t<h5 class=\"panel-title\">\n\t\t\t\t\t\t\t<a id=\"firstPanel\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">\n");
      out.write("\t\t\t\t\t\t\t\tSelect existing Profile\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</h5>\n\t\t\t\t\t</div>\n\t\t\t\t\n\t\t\t\t\t<div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n\t\t\t\t\t\t<div class=\"panel-body\">\n\t\t\t\t\t\t\t<form id=\"profile_form\" class=\"wizard-big\" method=\"post\">\n\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${_csrf.parameterName}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${_csrf.token}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\" />\n\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"sourceProfile\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${profileName}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\">\n\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"targetProfile\">\n\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"metadataObjects\">\n\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"includeDeleted\">\n\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-lg-12\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t<!-- <label>First name *</label>\n\t\t\t\t\t\t\t\t\t\t\t --><div class=\"input-group col-lg-9\">\n\t\t\t\t\t\t\t\t\t\t\t\t<select id=\"profile_selected\" name=\"profile_selected\" class=\"form-control\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"-1\">Select Profile</option>\n\t\t\t\t\t\t\t\t\t\t\t\t\t");
 for(String profile : profileList) { 
														if(profile.equals((String)request.getAttribute("profileName"))) continue; 
      out.write("\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"");
      out.print( profile);
      out.write('"');
      out.write('>');
      out.print( profile);
      out.write("</option>\n\t\t\t\t\t\t\t\t\t\t\t\t\t");
 } 
      out.write("\n\t\t\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\n\t\t\t\t\t\t\t</form>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t\n\t\t\t\t<div class=\"panel panel-default\">\n\t\t\t\t\t<div class=\"panel-heading\">\n\t\t\t\t\t\t<h4 class=\"panel-title\">\n\t\t\t\t\t\t\t<a id=\"secondPanel\" data-toggle=\"collapse\"\n\t\t\t\t\t\t\t   data-parent=\"#accordion\"\n\t\t\t\t\t\t\t   href=\"#collapseTwo\">Create a Profile</a>\n\t\t\t\t\t\t</h4>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div id=\"collapseTwo\" class=\"panel-collapse collapse \">\n\t\t\t\t\t\t<div class=\"panel-body\">\n\t\t\t\t\t\t\t<form id=\"profile_form\" class=\"wizard-big\" method=\"post\">\n\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${_csrf.parameterName}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${_csrf.token}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\" />\n\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"sourceProfile\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${profileName}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\">\n\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"targetProfile\">\n\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"metadataObjects\">\n\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"includeDeleted\">\n\t\t\t\t\t\t\t\t<div id=\"new_sf_page\"></div>\n\t\t\t\t\t\t\t</form>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_005furl_005f0(javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_005furl_005f0 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _005fjspx_005ftagPool_005fc_005furl_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    boolean _jspx_th_c_005furl_005f0_reused = false;
    try {
      _jspx_th_c_005furl_005f0.setPageContext(_jspx_page_context);
      _jspx_th_c_005furl_005f0.setParent(null);
      // /WEB-INF/views/export/listprofiles.jsp(64,10) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005furl_005f0.setValue("/profile/load/");
      int _jspx_eval_c_005furl_005f0 = _jspx_th_c_005furl_005f0.doStartTag();
      if (_jspx_th_c_005furl_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
      _005fjspx_005ftagPool_005fc_005furl_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005furl_005f0);
      _jspx_th_c_005furl_005f0_reused = true;
    } finally {
      org.apache.jasper.runtime.JspRuntimeLibrary.releaseTag(_jspx_th_c_005furl_005f0, _jsp_getInstanceManager(), _jspx_th_c_005furl_005f0_reused);
    }
    return false;
  }

  private boolean _jspx_meth_c_005furl_005f1(javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_005furl_005f1 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _005fjspx_005ftagPool_005fc_005furl_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    boolean _jspx_th_c_005furl_005f1_reused = false;
    try {
      _jspx_th_c_005furl_005f1.setPageContext(_jspx_page_context);
      _jspx_th_c_005furl_005f1.setParent(null);
      // /WEB-INF/views/export/listprofiles.jsp(148,11) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005furl_005f1.setValue("/profile/test/salesforce");
      int _jspx_eval_c_005furl_005f1 = _jspx_th_c_005furl_005f1.doStartTag();
      if (_jspx_th_c_005furl_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
      _005fjspx_005ftagPool_005fc_005furl_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005furl_005f1);
      _jspx_th_c_005furl_005f1_reused = true;
    } finally {
      org.apache.jasper.runtime.JspRuntimeLibrary.releaseTag(_jspx_th_c_005furl_005f1, _jsp_getInstanceManager(), _jspx_th_c_005furl_005f1_reused);
    }
    return false;
  }

  private boolean _jspx_meth_c_005furl_005f2(javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_005furl_005f2 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _005fjspx_005ftagPool_005fc_005furl_0026_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    boolean _jspx_th_c_005furl_005f2_reused = false;
    try {
      _jspx_th_c_005furl_005f2.setPageContext(_jspx_page_context);
      _jspx_th_c_005furl_005f2.setParent(null);
      // /WEB-INF/views/export/listprofiles.jsp(210,10) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005furl_005f2.setValue("/export/saveprofile");
      int _jspx_eval_c_005furl_005f2 = _jspx_th_c_005furl_005f2.doStartTag();
      if (_jspx_th_c_005furl_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
      _005fjspx_005ftagPool_005fc_005furl_0026_005fvalue_005fnobody.reuse(_jspx_th_c_005furl_005f2);
      _jspx_th_c_005furl_005f2_reused = true;
    } finally {
      org.apache.jasper.runtime.JspRuntimeLibrary.releaseTag(_jspx_th_c_005furl_005f2, _jsp_getInstanceManager(), _jspx_th_c_005furl_005f2_reused);
    }
    return false;
  }
}
